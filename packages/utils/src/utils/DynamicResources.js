export class DynamicResource {
    data = undefined
    status = 'pending'
    error = undefined
    promise = null

    read() {
        switch (this.status) {
            case 'pending':
                throw this.promise
            case 'error':
                throw this.error
            default:
                return this.data
        }
    }
}

export class AsyncResource extends DynamicResource {
    constructor(promise) {
        super()
        this.promise = promise
            .then((data) => {
                this.status = 'success'
                this.data = data
            })
            .catch((error) => {
                this.status = 'error'
                this.error = error
            })
    }
}

// eslint-disable-next-line no-unused-vars
class CacheOptions {
    persistent = undefined
    cacheDuration = undefined
}

export class CacheableAsyncResource extends AsyncResource {
    /**
     *
     * @param cacheKey {string} cache name
     * @param promiseFactory {function():Promise}
     * @param options {CacheOptions?}
     */
    constructor(cacheKey, promiseFactory, options) {
        const storage = !(options?.persistent || false) ? sessionStorage : localStorage
        let cachedItem = undefined
        try {
            cachedItem = JSON.parse(storage.getItem('cacheableResource-' + cacheKey))
            if (cachedItem && options?.cacheDuration) {
                if ((cachedItem.saved || 0) + options.cacheDuration * 1000 < Date.now()) {
                    cachedItem = undefined
                }
            }
        } catch (e) {
            console.log(e)
        }
        let innerPromise
        if (cachedItem) {
            innerPromise = Promise.resolve(cachedItem.data)
        } else {
            innerPromise = promiseFactory()
                .then(data => {
                    this.storeData(storage, data, cacheKey)
                    return data
                })
        }
        super(innerPromise)
    }

    storeData(storage, data, cacheKey) {
        storage.setItem('cacheableResource-' + cacheKey, JSON.stringify({
            data: data,
            saved: Date.now()
        }))
    }
}