const urlProps = {
    baseUrl: undefined,
    absoluteBaseUrl: undefined
}

/**
 * Tries to read context path from HTML page that opens React app
 *
 * @returns {string} Slash ended ('/') context path of application
 */
export function getBaseUrl() {
    if (!urlProps.baseUrl) {
        let baseUrl = document.querySelector('meta[name="baseUrl"]').content
        if (!baseUrl) {
            baseUrl = '/'
        }
        if (!baseUrl.endsWith('/')) {
            baseUrl += '/'
        }
        urlProps.baseUrl = baseUrl
    }
    return urlProps.baseUrl
}

/**
 * Adds base url to relative urls that are host relative
 *
 * @param url {string}
 * @returns {string} Slash ended ('/') context path of application
 */
export function addBaseUrl(url) {
    return url.startsWith('/') ? getBaseUrl() + url.substring(1) : url
}

export function addAbsoluteBaseUrl(url) {
    return new URL(window.location.href).origin + getBaseUrl() + (url.startsWith('/') ? url.substring(1) : url)
}

/**
 * Tries to read absolute base url of app from HTML page that opens React app
 *
 * @returns {string} Slash ended ('/') absolute base url of application
 */
export function getAbsoluteBaseUrl() {
    if (!urlProps.baseUrl) {
        let baseUrl = document.querySelector('meta[name="absoluteBaseUrl"]').content
        if (!baseUrl) {
            baseUrl = window.location.protocol + '//' + window.location.host + '/' + getBaseUrl()
        } else if (!baseUrl.endsWith('/')) {
            baseUrl += '/'
        }
        urlProps.absoluteBaseUrl = baseUrl
    }
    return urlProps.absoluteBaseUrl
}