import {fetchWithCsrf} from './CsrfUtils'
import {addBaseUrl} from './UrlUtils'

/**
 * Decorator for fetch method.
 * It automatically adds anti-XSRF token to request and reads and remembers new XSRF token
 * if one is returned with request. It also converts relative urls to have baseUrl if needed
 *
 * @param url {RequestInfo}
 * @param options {RequestInit}
 * @returns {Promise<Response>}
 */
export function AppFetch(url, options = undefined) {
    return fetchWithCsrf(addBaseUrl(url), options)
}