const csrfConfig = {
    header: undefined,
    value: undefined
}

/**
 * Reads anti-XSRF token from memory or from HTML page and creates header object from it
 * @returns {{}} header object with anti-XSRF token
 */
export function getCsrfHeader() {
    let csrfTokenName = csrfConfig.header
    if (!csrfTokenName) {
        csrfTokenName = document.querySelector('meta[name="csrfHeader"]').content
        csrfConfig.header = csrfTokenName
    }
    let csrfTokenValue = csrfConfig.value
    if (!csrfTokenValue) {
        csrfTokenValue = document.querySelector('meta[name="csrfToken"]').content
        csrfConfig.value = csrfTokenValue
    }
    return {[csrfTokenName]: csrfTokenValue}
}

/**
 * Decorator for fetch method.
 * It automatically adds anti-XSRF token to request and reads and remembers new XSRF token
 * if one is returned with request.
 *
 * @param url {RequestInfo}
 * @param options {RequestInit}
 * @returns {Promise<Response>}
 */
export function fetchWithCsrf(url, options = undefined) {
    if (options === undefined || options.method === 'GET') {
        return fetch(url, options)
    } else {
        const newConfig = {...options}
        newConfig.headers = newConfig.headers ? {...getCsrfHeader(), ...newConfig.headers} : getCsrfHeader()
        return fetch(url, newConfig)
            .then(response => {
                if (response.headers.has(csrfConfig.header)) {
                    csrfConfig.value = response.headers.get(csrfConfig.header)
                }
                return response
            })
    }
}