const TerserPlugin = require('terser-webpack-plugin')

module.exports = function (_env) {
    const mode = _env.dev ? 'development' : 'production'
    return {
        mode: mode,
        devtool: 'source-map',
        target: 'web',
        entry: {
            ReactDynamicComponent: './src/ReactDynamicComponent',
            index: './src/index.js'
        },
        output: {
            path: __dirname + '/dist',
            libraryTarget: 'module',
            globalObject: 'globals'
        },
        externals: {
            react: 'global react',
        },
        experiments: {
            outputModule: true
        },
        plugins: [].filter(Boolean),
        module: {
            rules: [
                {
                    test: /\.jsx?$/,
                    exclude: /node_modules/,
                    use: [{
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            cacheCompression: true,
                            presets: ['@babel/preset-env', '@babel/preset-react'],
                            comments: true
                        },
                    }]
                },
                {
                    test: /\.css$/i,
                    use: ['style-loader', 'css-loader'],
                }
            ]
        },
        resolve: {
            extensions: ['.js', '.jsx'],
        },
        optimization: {
            minimizer: [
                new TerserPlugin({
                    terserOptions: {
                        format: {
                            comments: (note, comment) => {
                                return comment.value === 'webpackIgnore:true'
                            }
                        }
                    }
                })
            ]
        }
    }
}

