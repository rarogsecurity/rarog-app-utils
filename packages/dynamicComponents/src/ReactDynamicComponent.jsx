import React, {useEffect, useRef, useState} from 'react'
import * as ReactIs from 'react-is'
import PropTypes from 'prop-types'
import './ReactDynamicComponent.css'

function Renderable({render, ...props}) {
    const ref = useRef(null)

    useEffect(() => {
        const dispose = render(ref.current, props)
        if (typeof dispose === 'function') {
            return dispose
        }
    }, [])

    return (
        <>
            <div ref={ref}></div>
        </>
    )
}

Renderable.propTypes = {
    render: PropTypes.func.isRequired
}

export const IFrameMode = {
    DISALLOWED: 0,
    ALLOWED_LOCAL: 1,
    ALLOWED_REMOTE: 2
}

function DynamicJsComponent({resource, fallback, ...props}) {
    const Component = React.lazy(() => import(/*webpackIgnore:true*/resource)
        .then(module => {
            if (typeof module.render === 'function') {
                return (<Renderable render={module.render} {...props}/>)
            } else if (ReactIs.isValidElementType(module.default)) {
                const Component = module.default
                return (<Component {...props}/>)
            } else {
                throw new Error('Module is not valid renderable module (doesn\'t export method `render` and doesn\'t export react component as default)')
            }
        })
        .then(component => { // wrap it nicely for React.lazy
            return {default: () => component}
        }))
    return (
        <React.Suspense fallback={fallback}>
            <Component/>
        </React.Suspense>
    )
}

DynamicJsComponent.propTypes = {
    resource: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(URL)]).isRequired,
    fallback: PropTypes.element.isRequired
}

function IframeWithFallback({resource, iframeMode, fallback}) {
    const [showFallback, setShowFallback] = useState(true)
    const iframeCsp = iframeMode === IFrameMode.ALLOWED_REMOTE ? undefined : 'default-src: \'self\''
    const iframeSandbox = iframeMode === IFrameMode.ALLOWED_REMOTE ? 'allow-scripts; allow-forms; allow-downloads' : undefined
    const fallbackFragment = (<div className="fallback">{fallback}</div>)
    return (
        <>
            <div className="dynamicComponentIframe">
                {showFallback ? fallbackFragment : undefined}
                <iframe src={resource.toString()}
                        referrerPolicy="origin-when-cross-origin"
                        sandbox={iframeSandbox}
                        csp={iframeCsp}
                        onLoad={() => setShowFallback(false)}
                        allow="autoplay=*;picture-in-picture=*;speaker-selection=self;camera=self;microphone=self;document-domain=self;encrypted-media=self;geolocation=self;idle-detection=self;"
                />
            </div>
        </>
    )
}

IframeWithFallback.propTypes = {
    resource: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(URL)]).isRequired,
    fallback: PropTypes.element.isRequired,
    iframeMode: PropTypes.oneOf(Object.keys(IFrameMode).map(key => IFrameMode[key]))
}

export default function DynamicComponent({resource, iframeMode, fallback, ...props}) {
    if (iframeMode === undefined) {
        iframeMode = IFrameMode.ALLOWED_LOCAL
    }
    if (typeof resource === 'string') {
        resource = new URL(resource, window.origin)
    }
    const pathname = resource.pathname
    const isCrossOrigin = resource.origin !== window.origin
    if (pathname.endsWith('.js') || pathname.endsWith('.jsx')) {
        if (isCrossOrigin) {
            throw new Error('Cross origin JS components are not allowed as they may contains malicious code')
        }
        return (<DynamicJsComponent resource={resource} fallback={fallback} {...props}/>)
    } else {
        if (iframeMode === IFrameMode.DISALLOWED) {
            throw new Error('Provided resource is not valid js component and iframe elements are not allowed')
        }
        if (iframeMode === IFrameMode.ALLOWED_LOCAL && isCrossOrigin) {
            throw new Error('Provided resource is pointing to remote resource and remote iframes are not allowed')
        }
        return (<IframeWithFallback resource={resource} iframeMode={iframeMode} fallback={fallback}/>)
    }
}

DynamicComponent.propTypes = {
    resource: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(URL)]).isRequired,
    fallback: PropTypes.element.isRequired,
    iframeMode: PropTypes.oneOf(Object.keys(IFrameMode).map(key => IFrameMode[key]))
}
