import {render, screen, waitFor, waitForElementToBeRemoved} from '@testing-library/react'
import DynamicComponent, {IFrameMode} from '../src/ReactDynamicComponent'
import {jest} from '@jest/globals'
import React from 'react'

describe('React DynamicComponent', () => {
    afterEach(() => {
        jest.clearAllMocks()
    })

    it('loads properly local react component', async () => {
        const mockComponent = () => (<React.Fragment>
            <div data-testid="MockedComponent">Mock React Component</div>
        </React.Fragment>)

        jest.mock('http://localhost/reactMock.jsx', () => {
            return mockComponent
        }, {virtual: true})

        render(
            <div data-testid="body">
                <DynamicComponent fallback={<>Loading...</>} resource="reactMock.jsx"/>
            </div>, {}
        )

        await waitFor(() => screen.getByTestId('MockedComponent'))

        expect(screen.getByTestId('body')).toMatchSnapshot()
    })

    it('loads react component with additional props', async () => {
        const mockComponent = ({test}) => (<React.Fragment>
            <div data-testid="MockedComponent">Mock React Component. Property value: {test}</div>
        </React.Fragment>)

        jest.mock('http://localhost/reactMockWithProps.jsx', () => {
            return mockComponent
        }, {virtual: true})

        render(
            <div data-testid="body">
                <DynamicComponent fallback={<>Loading...</>} resource="reactMockWithProps.jsx" test={"TestValue"}/>
            </div>, {}
        )

        await waitFor(() => screen.getByTestId('MockedComponent'))

        expect(screen.getByTestId('body')).toMatchSnapshot()
    })

    it('loads properly local module exporting renderer', async () => {
        const mockRender = (domElement) => {
            const div = document.createElement('div')
            div.setAttribute('data-testid', 'MockedComponent')
            div.innerHTML = 'Mock Generic Render Component'
            domElement.append(div)
        }

        jest.mock('http://localhost/renderMock.jsx', () => ({
            render: mockRender
        }), {virtual: true})

        render(
            <div data-testid="body">
                <DynamicComponent fallback={<>Loading...</>} resource="renderMock.jsx"/>
            </div>, {}
        )

        await waitFor(() => screen.getByTestId('MockedComponent'))

        expect(screen.getByTestId('body')).toMatchSnapshot()
    })

    it('loads properly renderer with additional props', async () => {
        const mockRender = (domElement, props) => {
            const div = document.createElement('div')
            div.setAttribute('data-testid', 'MockedComponent')
            div.innerHTML = 'Mock Generic Render Component. Property value: ' + props.test
            domElement.append(div)
        }

        jest.mock('http://localhost/renderMockWithProps.jsx', () => ({
            render: mockRender
        }), {virtual: true})

        render(
            <div data-testid="body">
                <DynamicComponent fallback={<>Loading...</>} resource="renderMockWithProps.jsx" test="TestValue"/>
            </div>, {}
        )

        await waitFor(() => screen.getByTestId('MockedComponent'))

        expect(screen.getByTestId('body')).toMatchSnapshot()
    })

    it('prevents js load from different origin', async () => {
        expect(() => render(
            <div data-testid="body">
                <DynamicComponent fallback={<>Loading...</>} resource="http://malicious.domain/mock.jsx"/>
            </div>
        )).toThrowError('Cross origin JS components are not allowed as they may contains malicious code')
    })

    it('renders remote iframe', async () => {
        const iframeSrc = 'file://' + __dirname + '/mocks/page.html'
        render(
            <div data-testid="body">
                <DynamicComponent fallback={<>
                    <div data-testid="fallback">Loading...</div>
                </>} resource={iframeSrc} iframeMode={IFrameMode.ALLOWED_REMOTE}/>
            </div>
        )

        await waitForElementToBeRemoved(() => screen.getByTestId('fallback'))

        expect(screen.getByTestId('body')).toMatchSnapshot()
    })

    it('renders local iframe', async () => {
        render(
            <div data-testid="body">
                <DynamicComponent fallback={<>
                    <div data-testid="fallback">Loading...</div>
                </>} resource="http://localhost/iframe" iframeMode={IFrameMode.ALLOWED_LOCAL}/>
            </div>
        )

        expect(screen.getByTestId('body')).toMatchSnapshot()
    })

    it('renders local iframe', async () => {
        expect(() => render(
            <div data-testid="body">
                <DynamicComponent fallback={<>
                    <div data-testid="fallback">Loading...</div>
                </>} resource="http://localhost/iframe" iframeMode={IFrameMode.DISALLOWED}/>
            </div>
        )).toThrowError('Provided resource is not valid js component and iframe elements are not allowed')
    })
})